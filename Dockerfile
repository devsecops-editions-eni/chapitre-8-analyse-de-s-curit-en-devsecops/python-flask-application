FROM python:3.13.1

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt \
    && addgroup user \
    && useradd -rm -d /home/user -s /bin/bash -g user -u 1001 user

COPY . .

EXPOSE 8080

USER user

CMD [ "python", "./main.py" ]